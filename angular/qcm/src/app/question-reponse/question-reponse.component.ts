import { Component, Input, OnInit } from '@angular/core';
import {CommonModule} from '@angular/common';
@Component({
  selector: 'question-reponse',
  templateUrl: './question-reponse.component.html',
  styleUrls: ['./question-reponse.component.scss']
})
export class QuestionReponseComponent implements OnInit {
  @Input() question: string = "ggg";
  @Input() comment: string = "sss";
  @Input() choices : Array<string> =[];
  @Input() responses: Array<number>=[];
  
  constructor() { }

  ngOnInit(): void {
  }

}
