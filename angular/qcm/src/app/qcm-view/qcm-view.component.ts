import { Component, OnInit } from '@angular/core';
import { QcmService } from '../services/qcm.service';
@Component({
  selector: 'app-qcm-view',
  templateUrl: './qcm-view.component.html',
  styleUrls: ['./qcm-view.component.scss']
})
export class QcmViewComponent implements OnInit {
  title = 'qcm2';
  questions: any[] = [];
  isAuth:boolean = false;

  showResults: boolean = false;
  constructor(private qcmService: QcmService) { }

  ngOnInit() {
    this.questions = this.qcmService.questions;
  }
  onFinish() {
    console.log("finish .....");
    this.showResults = true;
  }
}
