import { NgModule } from '@angular/core';

import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { QuestionReponseComponent } from './question-reponse/question-reponse.component';
import { ChoixComponent } from './choix/choix.component';
import {QcmService } from './services/qcm.service';
import {AuthService} from './services/auth.service';
import { AuthComponent } from './auth/auth.component';
import { QcmViewComponent } from './qcm-view/qcm-view.component';
import {RouterModule, Routes} from '@angular/router';

const appRoutes: Routes = [
  { path: 'qcm', component: QcmViewComponent },
  { path: 'auth', component: AuthComponent },
  { path: '', component: QcmViewComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    QuestionReponseComponent,
    ChoixComponent,
    AuthComponent,
    QcmViewComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [QcmService,AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
