export class QcmService {
    questions = [
        {
          question: 'QQuelle est la commande CLI qui permet de créer un nouveau component ?',
          choices: ["ng generate", "ng new", "ng build"],
          reponses: [0],     
          comment: 'ng new  crée un nouveau projet ;  ng build  crée le package global d\'un projet terminé.'
        },
        {
          question: 'Si vous avez une variable  name = \'Jonathan\'  dans votre code TypeScript, quelle syntaxe utilisez-vous pour afficher  Jonathan  dans le template ?',
          choices: ["les doubles accolades  {{}}", "les crochets  []", "Fiat"],
          reponses: [0],     
          comment: 'Les doubles accolades  {{}}  correspondent au string interpolation, permettant d\'afficher le résultat d\'une expression TypeScript dans le template — ici, une variable.'
        },
        {
          question: 'Vous souhaitez qu\'un bouton de votre template déclenche une fonction dans votre TypeScript : quelle syntaxe utilisez-vous pour l\'y lier ?',
          choices: ["(onClick)", "(click)", "[click]"],
          reponses: [1],     
          comment: 'En effet, pour lier à l\'événement  click  d\'un élément, on utilise la syntaxe  (click)  .'
        },
        {
          question: "Pour qu'un bouton soit désactivé quand la valeur  authenticated  est  false  , quelle syntaxe devez-vous utiliser ?",
          choices: ['<button disabled="!authenticated">', '<button (disabled)="!authenticated">', '<button [disabled]="!authenticated">'],
          reponses: [2],     
      comment: "Il faut utiliser les crochets  []  pour lier l'expression conditionnelle à la propriété  disabled  du bouton."
        },
        {
          question: 'Dans votre template, vous avez le sélecteur suivant : <app-my-component [firstName]="James"> Quelle ligne de code vous attendez-vous à voir dans le component correspondant ?',
          choices: ['const firstName: string;', '@Input() firstName: string;', 'firstName() { }'],
          reponses: [1],     
          comment: "La propriété firstName est une propriété personnalisée du component  app-my-component .  Cela ne peut ni être une constante, ni une méthode — il s'agit donc d'un  @Input()  ."
        }, {
          question: "Dans quels cas ce paragraphe apparaîtra dans le DOM ? <p *ngIf=\"myCondition\">Allô monde !</p> Attention, plusieurs réponses sont possibles.",
          choices: ['myCondition = false', 'myCondition = true', 'myCondition: string;', 'myCondition: boolean;', 'myCondition = "aucune condition";'],
          reponses: [1,4],     
          comment: "Un élément qui comporte la directive  *ngIf  s'affiche si la condition donnée est une expression conditionnelle qui retourne  true  ."
        }, {
          question: "Quelle directive vous permet d'afficher autant de components  <app-draw>  qu'il y a de membres dans l'array  drawings  ?",
          choices: ['<app-draw *ngIf="drawings">', '<app-draw *ngFor="let drawing of drawings">', '<app-draw *ngFor="for drawing in drawings">', '<app-draw *ngFor="drawings">'],
          reponses: [1],     
          comment: "La directive  *ngFor  s'emploie comme un for loop classique : on utilisera donc let … of … ."
        }, {
          question: "Par quel terme devez-vous remplacer le mot  réponse  ci-dessous afin d'assurer que la classe  bigText  s'applique toujours à cet item ? <li [ngClass] = \"{bigText: réponse}\">",
          choices: [ 'active', 'on', 'true'],
          reponses: [2],     
          comment: "La directive  ngClass  prend un objet où les clés correspondent aux classes à appliquer et les valeurs sont des expressions conditionnelles qui retournent  true  ou  false  ."
        }, {
          question: "Comment apparaîtra ce texte dans le template ? {{ 'OpenServers' | uppercase }}",
          choices: ['OpenServers', 'OPENSERVERS', 'OS'],
          reponses: [1],     
          comment: "La Pipe  uppercase  transforme le texte en majuscules."
        }, {
          question: "Si  myDate  est un objet Date où la date est le 1/1/2001 et l'heure est 10h30, comment apparaîtra le texte suivant dans le template ? {{ myDate | lowercase | date: 'short' }}",
          choices: ['1/1/2001, 10:30 am', '1/1/2001, 10:30 AM', '01/01/2001, 10:30am'],
          reponses: [],     
          comment: "Aucune de ces réponses L'objet  myDate  étant une Date, y appliquer la Pipe  lowercase  retournera une erreur."
        }, 
      ];
}