import { Component } from '@angular/core';
import { QcmService } from './services/qcm.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})


export class AppComponent {
  title = 'qcm1';
  questions: any[] = [];

  showResults: boolean = false;

  constructor(private qcmService: QcmService) {

  }

  ngOnInit() {
    this.questions = this.qcmService.questions;
  }
  
 
}
